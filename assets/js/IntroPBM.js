
var link_flash = "assets/images/flash/";
(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1920,
	height: 1486,
	fps: 24,
	color: "#000000",
	manifest: [
		{src: link_flash + "images/Bitmap1.png", id:"Bitmap1"},
		{src: link_flash + "images/Bitmap2.png", id:"Bitmap2"},
		{src: link_flash + "images/CHARcopy2.png", id:"CHARcopy2"},
		{src: link_flash + "images/CHARcopy5.png", id:"CHARcopy5"},
		{src: link_flash + "images/ColorBalance1.jpg", id:"ColorBalance1"},
		{src: link_flash + "images/Group1copy.png", id:"Group1copy"},
		{src: link_flash + "images/Group1copy10.png", id:"Group1copy10"},
		{src: link_flash + "images/Group1copy11.png", id:"Group1copy11"},
		{src: link_flash + "images/Group1copy12.png", id:"Group1copy12"},
		{src: link_flash + "images/Group1copy2.png", id:"Group1copy2"},
		{src: link_flash + "images/Group1copy3.png", id:"Group1copy3"},
		{src: link_flash + "images/Group1copy4.png", id:"Group1copy4"},
		{src: link_flash + "images/Group1copy5.png", id:"Group1copy5"},
		{src: link_flash + "images/Group1copy6.png", id:"Group1copy6"},
		{src: link_flash + "images/Group1copy7.png", id:"Group1copy7"},
		{src: link_flash + "images/Group1copy8.png", id:"Group1copy8"},
		{src: link_flash + "images/Group1copy9.png", id:"Group1copy9"},
		{src: link_flash + "images/image1.png", id:"image1"},
		{src: link_flash + "images/Layer1.png", id:"Layer1"},
		{src: link_flash + "images/Layer2.png", id:"Layer2"},
		{src: link_flash + "images/Layer3.png", id:"Layer3"},
		{src: link_flash + "images/Layer4.png", id:"Layer4"},
		{src: link_flash + "images/Layer5.png", id:"Layer5"},
		{src: link_flash + "images/Layer6.png", id:"Layer6"},
		{src: link_flash + "images/mat.png", id:"mat"},
		{src: link_flash + "images/naruto.png", id:"naruto"},
		{src: link_flash + "images/taychinh.png", id:"taychinh"},
		{src: link_flash + "images/_10010510001.png", id:"_10010510001"},
		{src: link_flash + "images/_10010510002.png", id:"_10010510002"},
		{src: link_flash + "images/_10010510003.png", id:"_10010510003"},
		{src: link_flash + "images/_10010510004.png", id:"_10010510004"}
	]
};



// symbols:



(lib.Bitmap1 = function() {
	this.initialize(img.Bitmap1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,42,69);


(lib.Bitmap2 = function() {
	this.initialize(img.Bitmap2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,96,15);


(lib.CHARcopy2 = function() {
	this.initialize(img.CHARcopy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,698,838);


(lib.CHARcopy5 = function() {
	this.initialize(img.CHARcopy5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,523,771);


(lib.ColorBalance1 = function() {
	this.initialize(img.ColorBalance1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1920,1486);


(lib.Group1copy = function() {
	this.initialize(img.Group1copy);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,132,99);


(lib.Group1copy10 = function() {
	this.initialize(img.Group1copy10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,82);


(lib.Group1copy11 = function() {
	this.initialize(img.Group1copy11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,132,81);


(lib.Group1copy12 = function() {
	this.initialize(img.Group1copy12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,155,82);


(lib.Group1copy2 = function() {
	this.initialize(img.Group1copy2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,148,86);


(lib.Group1copy3 = function() {
	this.initialize(img.Group1copy3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,129,91);


(lib.Group1copy4 = function() {
	this.initialize(img.Group1copy4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,183,103);


(lib.Group1copy5 = function() {
	this.initialize(img.Group1copy5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,134,88);


(lib.Group1copy6 = function() {
	this.initialize(img.Group1copy6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,138,103);


(lib.Group1copy7 = function() {
	this.initialize(img.Group1copy7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,151,80);


(lib.Group1copy8 = function() {
	this.initialize(img.Group1copy8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,113);


(lib.Group1copy9 = function() {
	this.initialize(img.Group1copy9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,152,81);


(lib.image1 = function() {
	this.initialize(img.image1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,150,158);


(lib.Layer1 = function() {
	this.initialize(img.Layer1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.Layer2 = function() {
	this.initialize(img.Layer2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,48,49);


(lib.Layer3 = function() {
	this.initialize(img.Layer3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,27,74);


(lib.Layer4 = function() {
	this.initialize(img.Layer4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,34,43);


(lib.Layer5 = function() {
	this.initialize(img.Layer5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,38,53);


(lib.Layer6 = function() {
	this.initialize(img.Layer6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,49,34);


(lib.mat = function() {
	this.initialize(img.mat);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,77,15);


(lib.naruto = function() {
	this.initialize(img.naruto);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,510,879);


(lib.taychinh = function() {
	this.initialize(img.taychinh);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,105,117);


(lib._10010510001 = function() {
	this.initialize(img._10010510001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,350,350);


(lib._10010510002 = function() {
	this.initialize(img._10010510002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,350,350);


(lib._10010510003 = function() {
	this.initialize(img._10010510003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,350,350);


(lib._10010510004 = function() {
	this.initialize(img._10010510004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,350,350);


(lib.Symbol14 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap1();
	this.instance.setTransform(7,17.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(7,17.5,42,69);


(lib.Symbol10 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.mat();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,77,15);


(lib.Symbol9 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,49,34);


(lib.Symbol8 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,38,53);


(lib.Symbol7 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,34,43);


(lib.Symbol6 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,50,79);


(lib.Symbol5 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,48,49);


(lib.Symbol4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,27,74);


(lib.taychinh_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.taychinh();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,105,117);


(lib.Group1copy12_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy12();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,155,82);


(lib.Group1copy11_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy11();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,132,81);


(lib.Group1copy10_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy10();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,143,82);


(lib.Group1copy9_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy9();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,152,81);


(lib.Group1copy8_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy8();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,147,113);


(lib.Group1copy7_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy7();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,151,80);


(lib.Group1copy6_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,138,103);


(lib.Group1copy5_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,134,88);


(lib.Group1copy4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,183,103);


(lib.Group1copy3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,129,91);


(lib.Group1copy2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,148,86);


(lib.Group1copy_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Group1copy();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,132,99);


(lib.ColorBalance1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.ColorBalance1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1920,1486);


(lib.CHARcopy5_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.CHARcopy5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,523,771);


(lib.CHARcopy2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.CHARcopy2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,698,838);


(lib.shape2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Bitmap2();
	this.instance.setTransform(8.1,47.9);

	this.shape = new cjs.Shape();
	this.shape.graphics.bf(img.image1, null, new cjs.Matrix2D(0.843,0.008,-0.003,0.338,-62.7,-27.3)).s().p("Ap3EFIABirIAGgJIADAGQAaAjAWAAQANAAAJAKQAIAKAKAAIAFgDQADgCAEAAQADAAAhAYQAlAXATAAQALAAAFgFQAGgGAAgIIgCgIQAPAGAJgIQAHgGAAgHQAAgHgLgKIAFgGIAEgLQAAgKgWgUIA8AJQAHAHAbAHIABAAQACAGADABIAdAHIAGADIAuASIAeATQAYARAGAAQAIAAAFgGIAKgGIACgEIAHgDIABAAIAgAJQAKAAAFgIIADgIIAIADIAAACQAAAHARAIQABAGAKAHIgBAHQAAAKA3ARIA1AMIByAlQALAAAFgIQADgFAAgGQAAgRgZgLIAAgCQAAgJgHgFIAlADQAUAAACgPIAEgDQAEgDAAgHIAFgNIAKADQAAAOACAEQAEANAQAAQAMAAAKgOIABgBIADAJQAIAKANAAIBpAAIAEAAIBUAMQAJANAOAAIAngIQAHAAAEALQAFALASAAQAOAAAJgMIADgEIgBAygAIiCDIAAgSIAYgFIgMAXgADEBRIACgHIAygHIgOAMQgHAHgKAAgAHnBAIACAAIgBABgAG7A/IABAAIgBABgAgfiDIABgBIAlAFIACAAIgdAFQgEgHgHgCgApZiPQgFgGgFAAQgKAAgGAIIABiCIFdADIACAGQAKAMAcAAIADAAIAAACIABAJIgQgBIhmgSQhUgMAAAcQAAASAhACIBTAIIgXAEIgOAJIhGAAQgfgBgIAMQgFAIAEAKQhJAhgOgBQgLgLgHAAQgKAAgFAFIgHAJgAmKiWIBEgJIACADIgEAAIhCAHgAFgkAIAFgGIBXABIgtAFg");
	this.shape.setTransform(58.3,56.6);

	this.addChild(this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-5,29.3,126.5,54.6);


(lib._003 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 图层 1
	this.instance = new lib._10010510004();
	this.instance.setTransform(-172,-171);

	this.instance_1 = new lib._10010510003();
	this.instance_1.setTransform(-172,-171);

	this.instance_2 = new lib._10010510002();
	this.instance_2.setTransform(-172,-171);

	this.instance_3 = new lib._10010510001();
	this.instance_3.setTransform(-172,-171);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},2).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).to({state:[]},2).wait(2));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-172,-171,350,350);


(lib.tayfll = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Group1copy2_1();
	this.instance.setTransform(0.5,27.5,1,1,0,0,0,74,43);

	this.instance_1 = new lib.Group1copy3_1();
	this.instance_1.setTransform(-3,36,1,1,0,0,0,64.5,45.5);

	this.instance_2 = new lib.Group1copy4_1();
	this.instance_2.setTransform(0,3,1,1,0,0,0,91.5,51.5);

	this.instance_3 = new lib.Group1copy5_1();
	this.instance_3.setTransform(-0.5,7.5,1,1,0,0,0,67,44);

	this.instance_4 = new lib.Group1copy6_1();
	this.instance_4.setTransform(7.5,17,1,1,0,0,0,69,51.5);

	this.instance_5 = new lib.Group1copy7_1();
	this.instance_5.setTransform(1,14.5,1,1,0,0,0,75.5,40);

	this.instance_6 = new lib.Group1copy8_1();
	this.instance_6.setTransform(-2,-25,1,1,0,0,0,73.5,56.5);

	this.instance_7 = new lib.Group1copy9_1();
	this.instance_7.setTransform(-3.5,14,1,1,0,0,0,76,40.5);

	this.instance_8 = new lib.Group1copy10_1();
	this.instance_8.setTransform(-1,15.5,1,1,0,0,0,71.5,41);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).to({state:[{t:this.instance_2}]},3).to({state:[{t:this.instance_3}]},3).to({state:[{t:this.instance_4}]},3).to({state:[{t:this.instance_5}]},3).to({state:[{t:this.instance_6}]},3).to({state:[{t:this.instance_7}]},3).to({state:[{t:this.instance_8}]},3).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-73.5,-15.5,148,86);


(lib.Symbol13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol14();
	this.instance.setTransform(22,-0.7,1,1,0,0,0,27.3,36.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:28,regY:52,x:14.7,y:96},0).wait(1).to({x:6.7,y:177.4},0).wait(1).to({x:-1.3,y:258.8},0).wait(1).to({x:-9.3,y:340.4},0).wait(1).to({x:-17.3,y:421.8},0).wait(1).to({x:-25.3,y:503.2},0).wait(1).to({x:-33.3,y:584.7},0).wait(1).to({x:-41.3,y:666.2},0).wait(1).to({x:-49.3,y:747.7},0).wait(1).to({x:-57.3,y:829.1},0).wait(1).to({x:-65.3,y:910.6},0).wait(1).to({x:-71.3,y:972.4,alpha:0.667},0).wait(1).to({x:-77.4,y:1034.2,alpha:0.333},0).wait(1).to({x:-83.5,y:1096,alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1.7,-20,42,69);


(lib.sprite3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.shape2("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(61));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-5,29.3,126.5,54.6);


(lib.Layer6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol9();
	this.instance.setTransform(4.1,34.1,1,1,0,0,0,2.6,33.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:24.5,regY:17,rotation:-0.6,x:25.9,y:17.2},0).wait(1).to({rotation:-1.2,x:25.7,y:17},0).wait(1).to({rotation:-1.8,x:25.5,y:16.7},0).wait(1).to({rotation:-2.4,x:25.3,y:16.5},0).wait(1).to({rotation:-3,x:25.2,y:16.3},0).wait(1).to({rotation:-3.6,x:25,y:16.1},0).wait(1).to({rotation:-4.2,x:24.8,y:15.8},0).wait(1).to({rotation:-4.8,x:24.6,y:15.6},0).wait(1).to({rotation:-5.3,x:24.4,y:15.4},0).wait(1).to({rotation:-5.9,x:24.2,y:15.2},0).wait(1).to({rotation:-6.5,x:24,y:15},0).wait(1).to({rotation:-7.1,x:23.8,y:14.8},0).wait(1).to({rotation:-7.7,x:23.6,y:14.6},0).wait(1).to({rotation:-8.3,x:23.4,y:14.4},0).wait(1).to({rotation:-8.9,x:23.2,y:14.2},0).wait(1).to({rotation:-9.5,x:23,y:14},0).wait(1).to({rotation:-8.8,x:23.2,y:14.2},0).wait(1).to({rotation:-8.2,x:23.4,y:14.5},0).wait(1).to({rotation:-7.5,x:23.7,y:14.7},0).wait(1).to({rotation:-6.8,x:23.9,y:14.9},0).wait(1).to({rotation:-6.1,x:24.1,y:15.2},0).wait(1).to({rotation:-5.4,x:24.4,y:15.4},0).wait(1).to({rotation:-4.8,x:24.6,y:15.6},0).wait(1).to({rotation:-4.1,x:24.8,y:15.9},0).wait(1).to({rotation:-3.4,x:25,y:16.1},0).wait(1).to({rotation:-2.7,x:25.2,y:16.4},0).wait(1).to({rotation:-2,x:25.4,y:16.7},0).wait(1).to({rotation:-1.4,x:25.6,y:16.9},0).wait(1).to({rotation:-0.7,x:25.8,y:17.1},0).wait(1).to({rotation:0,x:26,y:17.4},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1.5,0.4,49,34);


(lib.Layer5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol8();
	this.instance.setTransform(21.2,49.6,1,1,0,0,0,21.2,49.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:19,regY:26.5,rotation:0.6,x:19.3,y:26.5},0).wait(1).to({rotation:1.2,x:19.5},0).wait(1).to({rotation:1.8,x:19.7},0).wait(1).to({rotation:2.4,x:19.9},0).wait(1).to({rotation:2.9,x:20.2,y:26.4},0).wait(1).to({rotation:3.5,x:20.4},0).wait(1).to({rotation:4.1,x:20.7,y:26.5},0).wait(1).to({rotation:4.7,x:20.9,y:26.4},0).wait(1).to({rotation:5.3,x:21.1,y:26.5},0).wait(1).to({rotation:5.9,x:21.4,y:26.4},0).wait(1).to({rotation:6.5,x:21.6,y:26.5},0).wait(1).to({rotation:7.1,x:21.8},0).wait(1).to({rotation:7.6,x:22.1},0).wait(1).to({rotation:8.2,x:22.3},0).wait(1).to({rotation:8.8,x:22.6},0).wait(1).to({rotation:9.4,x:22.8},0).wait(1).to({rotation:8.7,x:22.6},0).wait(1).to({rotation:8.1,x:22.3},0).wait(1).to({rotation:7.4,x:22},0).wait(1).to({rotation:6.7,x:21.7,y:26.4},0).wait(1).to({rotation:6,x:21.4},0).wait(1).to({rotation:5.4,x:21.1,y:26.5},0).wait(1).to({rotation:4.7,x:20.9,y:26.4},0).wait(1).to({rotation:4,x:20.6,y:26.5},0).wait(1).to({rotation:3.4,x:20.3,y:26.4},0).wait(1).to({rotation:2.7,x:20.1,y:26.5},0).wait(1).to({rotation:2,x:19.8},0).wait(1).to({rotation:1.3,x:19.6},0).wait(1).to({rotation:0.7,x:19.3},0).wait(1).to({rotation:0,x:19},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,38,53);


(lib.Layer4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol7();
	this.instance.setTransform(32.6,0.8,1,1,0,0,0,32.6,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:17,regY:21.5,rotation:-0.5,x:17.2,y:21.6},0).wait(1).to({rotation:-1,x:17.4,y:21.8},0).wait(1).to({rotation:-1.5,x:17.6,y:21.9},0).wait(1).to({rotation:-2,x:17.8,y:22},0).wait(1).to({rotation:-2.5,x:18,y:22.2},0).wait(1).to({rotation:-3,x:18.2,y:22.3},0).wait(1).to({rotation:-3.5,x:18.3,y:22.4},0).wait(1).to({rotation:-4,x:18.5,y:22.5},0).wait(1).to({rotation:-4.5,x:18.7,y:22.7},0).wait(1).to({rotation:-5,x:18.9,y:22.8},0).wait(1).to({rotation:-5.5,x:19,y:22.9},0).wait(1).to({rotation:-6,x:19.2,y:23},0).wait(1).to({rotation:-6.5,x:19.5,y:23.1},0).wait(1).to({rotation:-7,x:19.6,y:23.2},0).wait(1).to({rotation:-7.5,x:19.8,y:23.4},0).wait(1).to({rotation:-8,x:20.1,y:23.5},0).wait(1).to({rotation:-7.4,x:19.8,y:23.3},0).wait(1).to({rotation:-6.9,x:19.6,y:23.2},0).wait(1).to({rotation:-6.3,x:19.4,y:23.1},0).wait(1).to({rotation:-5.7,x:19.1,y:23},0).wait(1).to({rotation:-5.2,x:19,y:22.8},0).wait(1).to({rotation:-4.6,x:18.7,y:22.7},0).wait(1).to({rotation:-4,x:18.5,y:22.5},0).wait(1).to({rotation:-3.4,x:18.3,y:22.4},0).wait(1).to({rotation:-2.9,x:18.1,y:22.2},0).wait(1).to({rotation:-2.3,x:17.9,y:22.1},0).wait(1).to({rotation:-1.7,x:17.7,y:22},0).wait(1).to({rotation:-1.1,x:17.5,y:21.8},0).wait(1).to({rotation:-0.6,x:17.2,y:21.7},0).wait(1).to({rotation:0,x:17,y:21.5},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,34,43);


(lib.Layer3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol4();
	this.instance.setTransform(11.2,0.8,1,1,0,0,0,11.2,0.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:13.5,regY:37,rotation:0.5,x:13.2,y:37.1},0).wait(1).to({rotation:1,x:12.9},0).wait(1).to({rotation:1.5,x:12.6},0).wait(1).to({rotation:2,x:12.3},0).wait(1).to({rotation:2.4,x:11.9},0).wait(1).to({rotation:2.9,x:11.7},0).wait(1).to({rotation:3.4,x:11.4},0).wait(1).to({rotation:3.9,x:11},0).wait(1).to({rotation:4.4,x:10.7},0).wait(1).to({rotation:4.9,x:10.4},0).wait(1).to({rotation:5.4,x:10.1},0).wait(1).to({rotation:5.9,x:9.8},0).wait(1).to({rotation:6.4,x:9.5},0).wait(1).to({rotation:6.8,x:9.2},0).wait(1).to({rotation:7.3,x:8.9,y:37},0).wait(1).to({rotation:7.8,x:8.5},0).wait(1).to({rotation:8.3,x:8.2},0).wait(1).to({rotation:8.8,x:8,y:36.9},0).wait(1).to({rotation:9.3,x:7.6,y:37},0).wait(1).to({rotation:9.8,x:7.3,y:36.9},0).wait(1).to({rotation:10.3,x:7},0).wait(1).to({rotation:9.7,x:7.3},0).wait(1).to({rotation:9.1,x:7.8,y:37},0).wait(1).to({rotation:8.6,x:8.1},0).wait(1).to({rotation:8,x:8.4},0).wait(1).to({rotation:7.4,x:8.8,y:37.1},0).wait(1).to({rotation:6.8,x:9.2},0).wait(1).to({rotation:6.3,x:9.5},0).wait(1).to({rotation:5.7,x:9.9},0).wait(1).to({rotation:5.1,x:10.3},0).wait(1).to({rotation:4.6,x:10.6},0).wait(1).to({rotation:4,x:11},0).wait(1).to({rotation:3.4,x:11.4},0).wait(1).to({rotation:2.9,x:11.7},0).wait(1).to({rotation:2.3,x:12.1},0).wait(1).to({rotation:1.7,x:12.4},0).wait(1).to({rotation:1.1,x:12.8},0).wait(1).to({rotation:0.6,x:13.2},0).wait(1).to({rotation:0,x:13.5,y:37},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,27,74);


(lib.Layer2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol5();
	this.instance.setTransform(3.9,5.2,1,1,0,0,0,3.9,5.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:24,regY:24.5,rotation:0.2,x:23.9,y:24.6},0).wait(1).to({rotation:0.4,x:23.8,y:24.7},0).wait(1).to({rotation:0.6,y:24.8},0).wait(1).to({rotation:0.9,x:23.7},0).wait(1).to({rotation:1.1,x:23.6,y:24.9},0).wait(1).to({rotation:1.3,y:25},0).wait(1).to({rotation:1.5,x:23.5,y:25.1},0).wait(1).to({rotation:1.7,x:23.4},0).wait(1).to({rotation:1.9,y:25.2},0).wait(1).to({rotation:2.1,x:23.3,y:25.3},0).wait(1).to({rotation:2.3,x:23.2,y:25.4},0).wait(1).to({rotation:2.6,x:23.1},0).wait(1).to({rotation:2.8,x:23,y:25.5},0).wait(1).to({rotation:3,y:25.6},0).wait(1).to({rotation:3.2,x:22.9},0).wait(1).to({rotation:3.4,x:22.8,y:25.7},0).wait(1).to({rotation:3.6,x:22.7,y:25.8},0).wait(1).to({rotation:3.8,y:25.9},0).wait(1).to({rotation:4,x:22.6},0).wait(1).to({rotation:4.3,x:22.5,y:26},0).wait(1).to({rotation:4.5,y:26.1},0).wait(1).to({rotation:4.2,y:26},0).wait(1).to({rotation:4,x:22.6,y:25.9},0).wait(1).to({rotation:3.7,x:22.7,y:25.8},0).wait(1).to({rotation:3.5,x:22.8,y:25.7},0).wait(1).to({rotation:3.2,y:25.6},0).wait(1).to({rotation:3,x:23},0).wait(1).to({rotation:2.7,x:23.1,y:25.5},0).wait(1).to({rotation:2.5,x:23.2,y:25.4},0).wait(1).to({rotation:2.2,x:23.3},0).wait(1).to({rotation:2,y:25.3},0).wait(1).to({rotation:1.7,x:23.4,y:25.2},0).wait(1).to({rotation:1.5,x:23.5,y:25.1},0).wait(1).to({rotation:1.2,x:23.6,y:25},0).wait(1).to({rotation:1,x:23.7,y:24.9},0).wait(1).to({rotation:0.7,x:23.8,y:24.8},0).wait(1).to({rotation:0.5,y:24.7},0).wait(1).to({rotation:0.2,x:23.9,y:24.6},0).wait(1).to({rotation:0,x:24,y:24.5},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,48,49);


(lib.Layer1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol6();
	this.instance.setTransform(10.9,4.1,1,1,0,0,0,11.1,4.8);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:25,regY:39.5,rotation:-0.5,x:25.1,y:38.7},0).wait(1).to({rotation:-0.9,x:25.4,y:38.6},0).wait(1).to({rotation:-1.4,x:25.6,y:38.5},0).wait(1).to({rotation:-1.8,x:25.9,y:38.4},0).wait(1).to({rotation:-2.3,x:26.2,y:38.2},0).wait(1).to({rotation:-2.7,x:26.4,y:38.1},0).wait(1).to({rotation:-3.2,x:26.7,y:38},0).wait(1).to({rotation:-3.6,x:27,y:37.8},0).wait(1).to({rotation:-4.1,x:27.2,y:37.7},0).wait(1).to({rotation:-4.6,x:27.5,y:37.5},0).wait(1).to({rotation:-5,x:27.8,y:37.4},0).wait(1).to({rotation:-5.5,x:28,y:37.2},0).wait(1).to({rotation:-5.9,x:28.3,y:37.1},0).wait(1).to({rotation:-6.4,x:28.6,y:37},0).wait(1).to({rotation:-6.8,x:28.8,y:36.8},0).wait(1).to({rotation:-7.3,x:29.1,y:36.7},0).wait(1).to({rotation:-6.8,x:28.8,y:36.8},0).wait(1).to({rotation:-6.2,x:28.5,y:37},0).wait(1).to({rotation:-5.7,x:28.2,y:37.2},0).wait(1).to({rotation:-5.2,x:27.9,y:37.4},0).wait(1).to({rotation:-4.7,x:27.6,y:37.5},0).wait(1).to({rotation:-4.2,x:27.3,y:37.7},0).wait(1).to({rotation:-3.6,x:27,y:37.8},0).wait(1).to({rotation:-3.1,x:26.7,y:38},0).wait(1).to({rotation:-2.6,x:26.3,y:38.1},0).wait(1).to({rotation:-2.1,x:26.1,y:38.3},0).wait(1).to({rotation:-1.6,x:25.8,y:38.4},0).wait(1).to({rotation:-1,x:25.4,y:38.6},0).wait(1).to({rotation:-0.5,x:25.1,y:38.7},0).wait(1).to({rotation:0,x:24.8,y:38.8},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.2,-0.7,50,79);


(lib.mat_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy
	this.instance = new lib.Symbol10();
	this.instance.setTransform(38.5,7.5,1,1,0,0,0,38.5,7.5);
	this.instance.alpha = 0;
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).wait(1));

	// Layer 1
	this.instance_1 = new lib.Symbol10();
	this.instance_1.setTransform(38.5,7.5,1,1,0,0,0,38.5,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,77,15);


(lib.Symbol15 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy 2
	this.instance = new lib.Symbol13();
	this.instance.setTransform(190.7,61.9,0.743,0.743,0,-68.6,111.4,25.1,17.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},28).wait(5));

	// Layer 1 copy 3
	this.instance_1 = new lib.Symbol13();
	this.instance_1.setTransform(93.9,43.9,0.531,0.531,-29.3,0,0,25.1,17.2);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(2).to({_off:false},0).to({_off:true},28).wait(3));

	// Layer 1 copy
	this.instance_2 = new lib.Symbol13();
	this.instance_2.setTransform(11.3,57.9,0.531,0.531,15.4,0,0,25.1,17.2);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).wait(28));

	// Layer 1
	this.instance_3 = new lib.Symbol13();
	this.instance_3.setTransform(1.1,-26.8,1,1,59.7);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({_off:true},28).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-40.3,-35.4,259.3,118.8);


(lib.sprite4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.sprite3();
	this.instance.setTransform(51.8,66.6,1.997,0.344,-154.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1.95,scaleY:0.34,rotation:-154.7,x:114.3,y:91.1},1).to({scaleX:1.87,scaleY:0.32,rotation:-155.4,x:238.9,y:141.6},2).to({scaleX:1.43,scaleY:0.25,rotation:-158.7,x:924,y:418.8},11).to({scaleX:1.19,scaleY:0.21,rotation:-160.4,x:1295.3,y:571.2,alpha:0},7).to({_off:true},1).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-162.2,-65,235.6,126.9);


(lib.toc = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Layer1_1();
	this.instance.setTransform(138,121.5,1,1,0,0,0,25,39.5);

	// Layer 2
	this.instance_1 = new lib.Layer2_1();
	this.instance_1.setTransform(171,118.5,1,1,0,0,0,24,24.5);

	// Layer 3
	this.instance_2 = new lib.Layer3_1();
	this.instance_2.setTransform(44.5,118,1,1,0,0,0,13.5,37);

	// Layer 4
	this.instance_3 = new lib.Layer4_1();
	this.instance_3.setTransform(17,108.5,1,1,0,0,0,17,21.5);

	// Layer 5 copy
	this.instance_4 = new lib.Layer5_1();
	this.instance_4.setTransform(140.2,36.4,1,1,28.2,0,0,19.1,26.4);

	// Layer 5
	this.instance_5 = new lib.Layer5_1();
	this.instance_5.setTransform(82,26.5,1,1,0,0,0,19,26.5);

	// Layer 6 copy 3
	this.instance_6 = new lib.Layer6_1();
	this.instance_6.setTransform(30.7,66.8,0.972,0.972,0,-4.3,175.7,24.4,17);

	// Layer 6 copy 2
	this.instance_7 = new lib.Layer6_1();
	this.instance_7.setTransform(46.8,45.1,1.035,1.035,-94,0,0,24.4,16.9);

	// Layer 6 copy
	this.instance_8 = new lib.Layer6_1();
	this.instance_8.setTransform(112.7,25.5,0.919,0.919,-30.4,0,0,24.5,17.1);

	// Layer 5 copy 2
	this.instance_9 = new lib.Layer5_1();
	this.instance_9.setTransform(19,95.7,1,1,-99.4,0,0,19.1,26.5);

	// Layer 6
	this.instance_10 = new lib.Layer6_1();
	this.instance_10.setTransform(162.5,54,1,1,0,0,0,24.5,17);

	this.addChild(this.instance_10,this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-10.2,0,205.3,160.3);


(lib.naruto_1 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.toc();
	this.instance.setTransform(292.1,82.3,1,1,0,0,0,97.5,80.5);

	// Layer 1
	this.instance_1 = new lib.naruto();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,510,879);


(lib.Symbol17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy 3
	this.instance = new lib.sprite4();
	this.instance.setTransform(-357.3,-56.4,0.825,0.825,179.9);
	this.instance.compositeOperation = "lighter";
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(3).to({_off:false},0).to({_off:true},34).wait(7));

	// Layer 1 copy 2
	this.instance_1 = new lib.sprite4();
	this.instance_1.setTransform(-104.7,162.4,0.825,0.825,48.4);
	this.instance_1.compositeOperation = "lighter";
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(7).to({_off:false},0).to({_off:true},30).wait(7));

	// Layer 1 copy 5
	this.instance_2 = new lib.sprite4();
	this.instance_2.setTransform(-352.7,-10.3,0.795,0.795,136.8);
	this.instance_2.compositeOperation = "lighter";
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).wait(30));

	// Layer 1 copy
	this.instance_3 = new lib.sprite4();
	this.instance_3.setTransform(44.5,1.6,1.215,1.215);
	this.instance_3.compositeOperation = "lighter";
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(11).to({_off:false},0).to({_off:true},30).wait(3));

	// Layer 1 copy 4
	this.instance_4 = new lib.sprite4();
	this.instance_4.setTransform(-314.5,96.1,0.746,0.746,0,-23.7,156.3);
	this.instance_4.compositeOperation = "lighter";
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(3).to({_off:false},0).to({_off:true},30).wait(11));

	// Layer 1
	this.instance_5 = new lib.sprite4();
	this.instance_5.setTransform(-132.7,-63.6,0.746,0.746,0,163.1,-16.9);
	this.instance_5.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({_off:true},30).wait(14));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-238,-121.9,147.9,138.2);


(lib.Symbol12 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.toc();
	this.instance.setTransform(291.7,80.5,1,1,0,0,0,97.5,80.5);

	// Layer 1
	this.instance_1 = new lib.naruto();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,510,879);


(lib.Symbol11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol12();
	this.instance.setTransform(0,0,1,1,0,0,0,255,439.5);
	this.instance.alpha = 0;
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:436.7,y:-2.8,alpha:0.022},0).wait(1).to({alpha:0.044},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.089},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.156},0).wait(1).to({alpha:0.178},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.175},0).wait(1).to({alpha:0.15},0).wait(1).to({alpha:0.125},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.075},0).wait(1).to({alpha:0.05},0).wait(1).to({alpha:0.025},0).wait(1).to({alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-255,-439.5,510,879);


(lib.Narutofull = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_276 = function() {
		this.gotoAndPlay(77);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(276).call(this.frame_276).wait(1));

	// Layer 4 copy
	this.instance = new lib._003();
	this.instance.setTransform(291.5,321.6,1.73,1.73);
	this.instance.compositeOperation = "lighter";
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(256).to({_off:false},0).to({_off:true},8).wait(13));

	// Layer 4
	this.instance_1 = new lib._003();
	this.instance_1.setTransform(291.5,321.6,2.753,2.753);
	this.instance_1.compositeOperation = "lighter";
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(52).to({_off:false},0).to({_off:true},8).wait(217));

	// Group 1 copy 13
	this.instance_2 = new lib.Group1copy12_1();
	this.instance_2.setTransform(177.5,325,1,1,0,0,0,77.5,41);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(76).to({_off:false},0).wait(201));

	// Group 1 copy 14
	this.instance_3 = new lib.Group1copy11_1();
	this.instance_3.setTransform(402,323.5,1,1,0,0,0,66,40.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(76).to({_off:false},0).wait(201));

	// Group 1 copy 15
	this.instance_4 = new lib.tayfll();
	this.instance_4.setTransform(292.5,308.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(229).to({_off:false},0).to({_off:true},27).wait(21));

	// Group 1 copy 16
	this.instance_5 = new lib.Group1copy_1();
	this.instance_5.setTransform(297,290.5,1,1,0,0,0,66,49.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(228).to({_off:false},0).to({_off:true},1).wait(48));

	// tay chinh copy
	this.instance_6 = new lib.taychinh_1();
	this.instance_6.setTransform(285.5,292.5,1,1,0,0,0,52.5,58.5);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(256).to({_off:false},0).wait(21));

	// Group 1 copy 12
	this.instance_7 = new lib.Group1copy12_1();
	this.instance_7.setTransform(177.5,325,1,1,0,0,0,77.5,41);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({_off:true},76).wait(201));

	// Group 1 copy 11
	this.instance_8 = new lib.Group1copy11_1();
	this.instance_8.setTransform(402,323.5,1,1,0,0,0,66,40.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({_off:true},76).wait(201));

	// Group 1 copy 10
	this.instance_9 = new lib.tayfll();
	this.instance_9.setTransform(292.5,308.5);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(25).to({_off:false},0).to({_off:true},27).wait(225));

	// Group 1 copy
	this.instance_10 = new lib.Group1copy_1();
	this.instance_10.setTransform(297,290.5,1,1,0,0,0,66,49.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({_off:true},25).wait(252));

	// tay chinh
	this.instance_11 = new lib.taychinh_1();
	this.instance_11.setTransform(285.5,292.5,1,1,0,0,0,52.5,58.5);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(52).to({_off:false},0).to({_off:true},176).wait(49));

	// mat
	this.instance_12 = new lib.mat_1();
	this.instance_12.setTransform(291.5,183.5,1,1,0,0,0,38.5,7.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({_off:true},52).wait(225));

	// naruto copy
	this.instance_13 = new lib.Symbol11();
	this.instance_13.setTransform(255,439.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(277));

	// naruto
	this.instance_14 = new lib.naruto_1();
	this.instance_14.setTransform(255,439.5,1,1,0,0,0,255,439.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(277));

	// Layer 5
	this.instance_15 = new lib._003();
	this.instance_15.setTransform(270,386.3,2.728,2.728);
	this.instance_15.compositeOperation = "lighter";
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(77).to({_off:false},0).wait(200));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,510,879);


(lib.Full = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_81 = function() {
		this.gotoAndPlay(65);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(81).call(this.frame_81).wait(1));

	// Naruto full
	this.instance = new lib.Narutofull();
	this.instance.setTransform(925,531.5,0.129,0.129,0,0,0,255.1,439.7);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:291,regY:364.8,scaleX:0.19,scaleY:0.19,x:931.9,y:476.2},0).wait(1).to({scaleX:0.25,scaleY:0.25,x:934.1,y:430.5},0).wait(1).to({scaleX:0.32,scaleY:0.32,x:936.4,y:384.9},0).wait(1).to({scaleX:0.38,scaleY:0.38,x:938.6,y:339.3},0).wait(1).to({scaleX:0.44,scaleY:0.44,x:940.8,y:293.6},0).wait(1).to({scaleX:0.5,scaleY:0.5,x:943.1,y:248},0).wait(1).to({scaleX:0.56,scaleY:0.56,x:945.3,y:202.4},0).wait(1).to({scaleX:0.63,scaleY:0.63,x:947.5,y:156.8},0).wait(1).to({scaleX:0.69,scaleY:0.69,x:949.8,y:193.3},0).wait(1).to({scaleX:0.75,scaleY:0.75,x:952,y:229.8},0).wait(1).to({scaleX:0.81,scaleY:0.81,x:954.2,y:266.3},0).wait(1).to({scaleX:0.88,scaleY:0.88,x:956.5,y:302.7},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:958.7,y:339.2},0).wait(1).to({scaleX:1,scaleY:1,x:961,y:375.7},0).wait(51).to({y:375.4},0).wait(1).to({y:375.1},0).wait(1).to({y:374.9},0).wait(1).to({y:374.6},0).wait(1).to({y:374.3},0).wait(1).to({y:374.1},0).wait(1).to({y:373.8},0).wait(1).to({y:373.6},0).wait(1).to({y:373.3},0).wait(1).to({y:373.6},0).wait(1).to({y:373.9},0).wait(1).to({y:374.2},0).wait(1).to({y:374.5},0).wait(1).to({y:374.8},0).wait(1).to({y:375.1},0).wait(1).to({y:375.4},0).wait(1).to({y:375.7},0).wait(1));

	// CHAR copy 6
	this.instance_1 = new lib.CHARcopy5_1();
	this.instance_1.setTransform(659.5,442.5,1,1,0,0,0,261.5,385.5);
	this.instance_1.alpha = 0;
	this.instance_1.compositeOperation = "lighter";
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(64).to({_off:false},0).wait(1).to({alpha:0.04},0).wait(1).to({alpha:0.08},0).wait(1).to({alpha:0.12},0).wait(1).to({alpha:0.16},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.133},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.067},0).wait(1).to({alpha:0.033},0).wait(1).to({alpha:0},0).wait(7));

	// CHAR copy 7
	this.instance_2 = new lib.CHARcopy2_1();
	this.instance_2.setTransform(1366,419,1,1,0,0,0,349,418.9);
	this.instance_2.alpha = 0;
	this.instance_2.compositeOperation = "lighter";
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(71).to({_off:false},0).wait(1).to({regY:419,y:419.1,alpha:0.04},0).wait(1).to({alpha:0.08},0).wait(1).to({alpha:0.12},0).wait(1).to({alpha:0.16},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.16},0).wait(1).to({alpha:0.12},0).wait(1).to({alpha:0.08},0).wait(1).to({alpha:0.04},0).wait(1).to({alpha:0},0).wait(1));

	// CHAR copy 5
	this.instance_3 = new lib.CHARcopy5_1();
	this.instance_3.setTransform(827.6,274.5,0.866,0.866,0,0,0,261.5,385.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(58).to({_off:false},0).wait(1).to({scaleX:0.89,scaleY:0.89,x:799.6,y:302.5,alpha:0.167},0).wait(1).to({scaleX:0.91,scaleY:0.91,x:771.5,y:330.5,alpha:0.333},0).wait(1).to({scaleX:0.93,scaleY:0.93,x:743.5,y:358.5,alpha:0.5},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:715.5,y:386.5,alpha:0.667},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:687.5,y:414.5,alpha:0.833},0).wait(1).to({scaleX:1,scaleY:1,x:659.5,y:442.5,alpha:1},0).wait(18));

	// CHAR copy 2
	this.instance_4 = new lib.CHARcopy2_1();
	this.instance_4.setTransform(1212,265,0.873,0.873,0,0,0,349,418.9);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(58).to({_off:false},0).wait(1).to({regY:419,scaleX:0.89,scaleY:0.89,x:1237.6,y:290.8,alpha:0.167},0).wait(1).to({scaleX:0.92,scaleY:0.92,x:1263.3,y:316.5,alpha:0.333},0).wait(1).to({scaleX:0.94,scaleY:0.94,x:1289,y:342.1,alpha:0.5},0).wait(1).to({scaleX:0.96,scaleY:0.96,x:1314.7,y:367.8,alpha:0.667},0).wait(1).to({scaleX:0.98,scaleY:0.98,x:1340.3,y:393.5,alpha:0.833},0).wait(1).to({scaleX:1,scaleY:1,x:1366,y:419.1,alpha:1},0).wait(18));

	// Layer 11
	this.instance_5 = new lib.Symbol17();
	this.instance_5.setTransform(1055.9,453.2,1.271,1.271,0,0,0,-128.1,62.8);
	this.instance_5.alpha = 0.199;
	this.instance_5.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(82));

	// Layer 10 copy
	this.instance_6 = new lib.Symbol15();
	this.instance_6.setTransform(1457.1,79.7,1,1,180,0,0,-467.1,223.3);
	this.instance_6.alpha = 0.301;
	this.instance_6.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(82));

	// Layer 10
	this.instance_7 = new lib.Symbol15();
	this.instance_7.setTransform(453.3,552.6,1,1,0,0,0,-467.1,223.3);
	this.instance_7.alpha = 0.301;
	this.instance_7.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(82));

	// Color Balance 1
	this.instance_8 = new lib.ColorBalance1_1();
	this.instance_8.setTransform(960,743,1,1,0,0,0,960,743);

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(82));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1920,1486);


// stage content:
(lib.IntroPBM = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Full();
	this.instance.setTransform(960,743,1,1,0,0,0,960,743);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(960,743,1920,1486);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;