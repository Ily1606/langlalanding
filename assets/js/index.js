function Slider (data, elementNode, timmer = 3000) {
  let idTimmer = null,
  currentSlide = 0,
  currentTransform = 0
  const wrapSlide = document.createElement("div")
  wrapSlide.classList.add("slide")
  const contentSlide = document.createElement('div')
  contentSlide.classList.add('content-slide')
  data.forEach(element => {
    const slideItem = document.createElement('div')
    slideItem.classList.add('slide-item')
    const img = document.createElement("img")
    img.src = element.img
    slideItem.append(img)
    wrapSlide.append(slideItem)
  });
  contentSlide.append(wrapSlide)
  elementNode.append(contentSlide)
  const buttonPrev = document.createElement("div")
  buttonPrev.classList.add("prev-slide")
  buttonPrev.addEventListener("click", () => {
    prev()
    start()
  })
  elementNode.append(buttonPrev)

  const buttonNext = document.createElement("div")
  buttonNext.classList.add("next-slide")
  buttonNext.addEventListener("click", () => {
    next()
    start()
  })
  elementNode.append(buttonNext)

  const start = () => {
    if(idTimmer) {
      clearInterval(idTimmer)
    }
    idTimmer = setInterval(() => {
      next()
    }, timmer);
  }
  const next = () => {
    if(currentSlide === data.length - 1) {
      currentSlide = 0
      currentTransform = 0
    }
    else{
      ++currentSlide
      const nextNode = elementNode.querySelectorAll(".slide-item")[currentSlide]
      const width = nextNode.offsetWidth
      currentTransform += width
    }
    wrapSlide.setAttribute("style", `transform: translateX(-${currentTransform}px)`)
  }
  const prev = () => {
    if(currentSlide === 0) {
      currentSlide = data.length - 1
      currentTransform = 0
      const els = elementNode.querySelectorAll(".slide-item")
      for(let i = 1; i < els.length; i++) {
        const el = els[i]
        currentTransform += el.offsetWidth
      }
    }
    else{
      --currentSlide
      console.log(currentSlide);
      const nextNode = elementNode.querySelectorAll(".slide-item")[currentSlide]
      const width = nextNode.offsetWidth
      currentTransform -= width
    }
    wrapSlide.setAttribute("style", `transform: translateX(-${currentTransform}px)`)
  }
  start()
}

const frame2_slide = document.getElementById("slideframe2")

const dataSliderFrame2 = [{
  link: "#",
  img: "/assets/images/slide/frame_2/1.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_2/2.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_2/3.webp"
}]

const sliderFrame2 = new Slider(dataSliderFrame2, frame2_slide, 3000)


const frame3_slide = document.getElementById("frame3_slide")

const dataSliderFrame3 = [{
  link: "#",
  img: "/assets/images/slide/frame_3/1.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/2.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/3.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/4.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/5.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/6.webp"
},{
  link: "#",
  img: "/assets/images/slide/frame_3/7.webp"
}]

const sliderFrame3 = new Slider(dataSliderFrame3, frame3_slide, 3000)
   
var canvas, stage,stage2, stage3, exportRoot;

function init() {
   canvas = document.getElementById("canvas");
   images = images||{};

   var loader = new createjs.LoadQueue(false);
   loader.addEventListener("fileload", handleFileLoad);
   loader.addEventListener("complete", handleComplete);
   loader.loadManifest(lib.properties.manifest);
}

function handleFileLoad(evt) {
   if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function handleComplete(evt) {
   exportRoot = new lib.IntroPBM();

   stage = new createjs.Stage(canvas);
   stage.addChild(exportRoot);
   stage.update();

   createjs.Ticker.setFPS(lib.properties.fps);
   createjs.Ticker.addEventListener("tick", stage);
   init2();
}

function init2() {
   canvas = document.getElementById("canvas_f2");
   images = images||{};

   var loader = new createjs.LoadQueue(false);
   loader.addEventListener("fileload", handleFileLoad2);
   loader.addEventListener("complete", handleComplete2);
   loader.loadManifest(lib_f2.properties.manifest);
}

function handleFileLoad2(evt) {
   if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function handleComplete2(evt) {
   exportRoot = new lib_f2.slogan();

   stage2 = new createjs.Stage(canvas);
   stage2.addChild(exportRoot);
   stage2.update();

   createjs.Ticker.setFPS(lib_f2.properties.fps);
   createjs.Ticker.addEventListener("tick", stage2);
   init3();
}

function init3() {
   canvas = document.getElementById("canvas_f3");
   images = images||{};

   var loader = new createjs.LoadQueue(false);
   loader.addEventListener("fileload", handleFileLoad3);
   loader.addEventListener("complete", handleComplete3);
   loader.loadManifest(lib_f3.properties.manifest);
}

function handleFileLoad3(evt) {
   if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function handleComplete3(evt) {
   exportRoot = new lib_f3.muiten();

   stage3 = new createjs.Stage(canvas);
   stage3.addChild(exportRoot);
   stage3.update();

   createjs.Ticker.setFPS(lib_f3.properties.fps);
   createjs.Ticker.addEventListener("tick", stage3);
}
if(window.outerWidth > 1024) {
  window.onload = init()
}