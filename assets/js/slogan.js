(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 820,
	height: 722,
	fps: 24,
	color: "#000000",
	manifest: [
		{src: link_flash + "images/text.png", id:"text"}
	]
};



// symbols:



(lib.Project23_00000 = function() {
	this.initialize(img.Project23_00000);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00001 = function() {
	this.initialize(img.Project23_00001);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00002 = function() {
	this.initialize(img.Project23_00002);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00003 = function() {
	this.initialize(img.Project23_00003);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00004 = function() {
	this.initialize(img.Project23_00004);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00005 = function() {
	this.initialize(img.Project23_00005);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00006 = function() {
	this.initialize(img.Project23_00006);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00007 = function() {
	this.initialize(img.Project23_00007);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00008 = function() {
	this.initialize(img.Project23_00008);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00009 = function() {
	this.initialize(img.Project23_00009);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00010 = function() {
	this.initialize(img.Project23_00010);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00011 = function() {
	this.initialize(img.Project23_00011);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.Project23_00012 = function() {
	this.initialize(img.Project23_00012);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,820,722);


(lib.text = function() {
	this.initialize(img.text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,257,334);


(lib.Symbol21 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,257,334);


(lib.Symbol3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.text();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,257,334);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_12 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(12).call(this.frame_12).wait(1));

	// Layer 1
	this.instance = new lib.Project23_00000();
	this.instance.setTransform(-410,-361);

	this.instance_1 = new lib.Project23_00001();
	this.instance_1.setTransform(-410,-361);

	this.instance_2 = new lib.Project23_00002();
	this.instance_2.setTransform(-410,-361);

	this.instance_3 = new lib.Project23_00003();
	this.instance_3.setTransform(-410,-361);

	this.instance_4 = new lib.Project23_00004();
	this.instance_4.setTransform(-410,-361);

	this.instance_5 = new lib.Project23_00005();
	this.instance_5.setTransform(-410,-361);

	this.instance_6 = new lib.Project23_00006();
	this.instance_6.setTransform(-410,-361);

	this.instance_7 = new lib.Project23_00007();
	this.instance_7.setTransform(-410,-361);

	this.instance_8 = new lib.Project23_00008();
	this.instance_8.setTransform(-410,-361);

	this.instance_9 = new lib.Project23_00009();
	this.instance_9.setTransform(-410,-361);

	this.instance_10 = new lib.Project23_00010();
	this.instance_10.setTransform(-410,-361);

	this.instance_11 = new lib.Project23_00011();
	this.instance_11.setTransform(-410,-361);

	this.instance_12 = new lib.Project23_00012();
	this.instance_12.setTransform(-410,-361);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},1).to({state:[{t:this.instance_2}]},1).to({state:[{t:this.instance_3}]},1).to({state:[{t:this.instance_4}]},1).to({state:[{t:this.instance_5}]},1).to({state:[{t:this.instance_6}]},1).to({state:[{t:this.instance_7}]},1).to({state:[{t:this.instance_8}]},1).to({state:[{t:this.instance_9}]},1).to({state:[{t:this.instance_10}]},1).to({state:[{t:this.instance_11}]},1).to({state:[{t:this.instance_12}]},1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-410,-361,820,722);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol3();
	this.instance.setTransform(0,0,1,1,0,0,0,128.5,167);
	this.instance.alpha = 0;
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({alpha:0.05},0).wait(1).to({alpha:0.1},0).wait(1).to({alpha:0.15},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.3},0).wait(1).to({alpha:0.35},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.45},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.389},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.278},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.167},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.056},0).wait(1).to({alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-128.5,-167,257,334);


(lib.text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_214 = function() {
		this.gotoAndPlay(15);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(214).call(this.frame_214).wait(1));

	// Layer 9
	this.instance = new lib.Symbol1();
	this.instance.setTransform(119,178);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},14).wait(201));

	// Layer 1 copy
	this.instance_1 = new lib.Symbol2();
	this.instance_1.setTransform(128.5,167);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(14).to({_off:false},0).wait(194).to({x:130.5,y:165},0).wait(1).to({x:126.5,y:169},0).wait(1).to({y:165},0).wait(1).to({x:130.5,y:169},0).wait(1).to({x:128.5,y:167},0).wait(3));

	// Layer 1
	this.instance_2 = new lib.Symbol21();
	this.instance_2.setTransform(128.5,167,1,1,0,0,0,128.5,167);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(14).to({_off:false},0).wait(194).to({x:130.5,y:165},0).wait(1).to({x:126.5,y:169},0).wait(1).to({y:165},0).wait(1).to({x:130.5,y:169},0).wait(1).to({x:128.5,y:167},0).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-291,-183,820,722);


(lib.Full = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_81 = function() {
		this.gotoAndPlay(65);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(81).call(this.frame_81).wait(1));

	// text
	this.instance = new lib.text_1();
	this.instance.setTransform(397.5,350,1,1,0,0,0,128.5,167);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(58).to({_off:false},0).wait(24));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


// stage content:



(lib.slogan = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Full();
	this.instance.setTransform(960,743,1,1,0,0,0,960,743);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = null;

})(lib_f2 = lib_f2||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib_f2, images, createjs, ss;