(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 60,
	height: 95,
	fps: 24,
	color: "#666666",
	manifest: [
		{src: link_flash + "images/muiten_1.png", id:"muiten_1"}
	]
};



// symbols:



(lib.muiten_1 = function() {
	this.initialize(img.muiten_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,60,68);


(lib.Symbol25 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.muiten_1();
	this.instance.setTransform(0,68,1,1,0,180,0);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,60,68);


(lib.Symbol24 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol25();
	this.instance.setTransform(0,0,1,1,0,0,0,30,34);
	this.instance.alpha = 0;
	this.instance.compositeOperation = "lighter";

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30,-34,60,68);


(lib.Symbol23 = function() {
	this.initialize();

	// Layer 1 copy
	this.instance = new lib.Symbol24();

	// Layer 1
	this.instance_1 = new lib.muiten_1();
	this.instance_1.setTransform(-30,34,1,1,0,180,0);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-30,-34,60,68);


(lib.Symbol22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol23("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({y:0.5},0).wait(1).to({y:1},0).wait(1).to({y:1.5},0).wait(1).to({y:2},0).wait(1).to({y:2.5},0).wait(1).to({y:3},0).wait(1).to({y:3.5},0).wait(1).to({y:3.3},0).wait(1).to({y:3.1},0).wait(1).to({y:2.8},0).wait(1).to({y:2.6},0).wait(1).to({y:2.4},0).wait(1).to({y:2.2},0).wait(1).to({y:2},0).wait(1).to({y:1.8},0).wait(1).to({y:1.5},0).wait(1).to({y:1.3},0).wait(1).to({y:1.1},0).wait(1).to({y:0.9},0).wait(1).to({y:0.7},0).wait(1).to({y:0.4},0).wait(1).to({y:0.2},0).wait(1).to({y:0},0).wait(1));

	// Layer 1 copy 2
	this.instance_1 = new lib.Symbol23();
	this.instance_1.setTransform(0,3.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(11).to({_off:false},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:5.4,alpha:0.889},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:7.4,alpha:0.778},0).wait(1).to({scaleX:1.04,scaleY:1.04,y:9.3,alpha:0.667},0).wait(1).to({scaleX:1.06,scaleY:1.06,y:11.3,alpha:0.556},0).wait(1).to({scaleX:1.07,scaleY:1.07,y:13.2,alpha:0.444},0).wait(1).to({scaleX:1.09,scaleY:1.09,y:15.2,alpha:0.333},0).wait(1).to({scaleX:1.1,scaleY:1.1,y:17.1,alpha:0.222},0).wait(1).to({scaleX:1.12,scaleY:1.12,y:19.1,alpha:0.111},0).wait(1).to({scaleX:1.13,scaleY:1.13,y:21,alpha:0},0).to({_off:true},1).wait(3));

	// Layer 1 copy
	this.instance_2 = new lib.Symbol23();
	this.instance_2.setTransform(0,3.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(7).to({_off:false},0).wait(1).to({scaleX:1.01,scaleY:1.01,y:5.4,alpha:0.889},0).wait(1).to({scaleX:1.03,scaleY:1.03,y:7.4,alpha:0.778},0).wait(1).to({scaleX:1.04,scaleY:1.04,y:9.3,alpha:0.667},0).wait(1).to({scaleX:1.06,scaleY:1.06,y:11.3,alpha:0.556},0).wait(1).to({scaleX:1.07,scaleY:1.07,y:13.2,alpha:0.444},0).wait(1).to({scaleX:1.09,scaleY:1.09,y:15.2,alpha:0.333},0).wait(1).to({scaleX:1.1,scaleY:1.1,y:17.1,alpha:0.222},0).wait(1).to({scaleX:1.12,scaleY:1.12,y:19.1,alpha:0.111},0).wait(1).to({scaleX:1.13,scaleY:1.13,y:21,alpha:0},0).to({_off:true},1).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-30,-34,60,68);


// stage content:



(lib.muiten = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.Symbol22();
	this.instance.setTransform(30,34);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(30,47.5,60,68);

})(lib_f3 = lib_f3||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib_f3, images, createjs, ss;